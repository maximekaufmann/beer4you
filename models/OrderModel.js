module.exports = (_db)=>{
    db = _db
    return OrderModel
}

class OrderModel {
    //validation d'une commande (ajout)
    static saveOneOrder(userId, totalAmount){
        
        
    }
    
    //sauvegarde d'un orderDetail (sera appelée plusieurs fois par une boucle dans la route d'ajout d'une commande)
    static saveOneOrderDetail(orderId, beer){
        
        
    }
    
    //modification du montant total
    static updateTotalAmount(orderId, totalAmount){
        
        
        
    }
    
    //récupération d'une commande en fonction d'un id
    static getOneOrder(id){
        
        
        
    }
    
    //modification d'un status de commande
    static updateStatus(orderId, status){
        
        
        
    }
    
    //récupération de toutes les commandes
    static getAllOrders(){



    }
    
    //récupération des détails d'une commande
    static getAllDetails(orderId){
        
        
        
    }
}