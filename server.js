const express = require("express")
const app = express()

const mysql = require("promise-mysql")
const cors = require("cors")

app.use(cors())

const fileUpload = require('express-fileupload')

app.use(fileUpload({
    createParentPath: true
}))

//parse les url
app.use(express.urlencoded({extended: false}))
app.use(express.json())
app.use(express.static(__dirname+'/public'))


let config;
//on check si l'api est en ligne (sur un server) ou non et on décide sur quelle bdd on va brancher notre back
if(!process.env.HOST){
    //nous sommes en local
    config = require("./config-offline")
} else {
    //nous sommes en ligne
    config = require("./config-online")
}

//on prépare la connexion à la BDD
const host = process.env.HOST_DB || config.db.host
const database = process.env.DATABASE_DB || config.db.database
const user = process.env.USER_DB || config.db.user
const password = process.env.PASSWORD_DB || config.db.password
//const port = process.env.PORT || config.db.PORT

//on importe nos routes
const userRoutes =  require("./routes/userRoutes")
const authRoutes =  require("./routes/authRoutes")
const beerRoutes =  require("./routes/beerRoutes")
const orderRoutes =  require("./routes/orderRoutes")


mysql.createConnection({
    host: host,
    database: database,
    user: user,
    password: password
    //port: port //pour ceux qui utilisent mamp
}).then((db) => {
    console.log('connecté bdd cousin!')
    setInterval(async () => {
        const res = await db.query("SELECT 1")
    }, 10000)
    
    app.get('/', async (req, res, next) => {
        res.json({status: 200, msg: "Welcome to your API BEER4YOU my friend!"})
    })
    
    userRoutes(app, db)
    authRoutes(app, db)
    beerRoutes(app, db)
    orderRoutes(app, db)
})
.catch((err=>console.log(err)))

const PORT = process.env.PORT || 9500
app.listen(PORT, () => {
    console.log(`Serveur à l'écoute sur le port ${PORT} mon poulet!`)
})